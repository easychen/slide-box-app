<!-- Header -->
<div class="center-box">
    <h1>SlideBox</h1>
        <h3>from Slide to video</h3>
        <form method="POST" action="/convert" enctype="multipart/form-data">
  <div class="form-group">
    <label for="file-pptx">pptx 文件*</label>
    <input type="file" id="file-pptx" name="pptx" accept=".pptx" required="true">
    <p class="help-block">Keynote 请直接导出为 pptx。</p>
  </div>
  <div class="form-group">
    <label for="file-pdf">PDF 文件</label>
    <input type="file" id="file-pdf" name="pdf" accept=".pdf">
    <p class="help-block">可选，提供 PDF 文件可以更好的保持您本机的字体等效果。</p>
  </div>
  <div class="form-group">
    <label for="key">SlideBox APIKey*</label>
    <div class="input-group input-group-lg"><input type="text" id="key" name="apikey" class="full" required="true" value="<?=@$_SESSION['apikey']?>"></div>
    <p class="help-block">从方糖云获得</p>
  </div>

  <div class="form-group">
    语音合成引擎：<select name="engine" id="engine"><option value="baidu">百度</option><option value="xunfei">讯飞</option></select>
  </div>



  <div class="form-group">
    <label for="key">百度云语音合成 APIKey*</label>
    <div class="input-group input-group-lg"><input type="text" id="key" name="akey" class="full"  value="<?=@$_SESSION['akey']?>"></div>
    <p class="help-block">从<a href="https://ai.baidu.com/tech/speech/tts" target="_blank">百度云获得</a></p>
  </div>

  <div class="form-group">
    <label for="key">百度云语音合成 API secret*</label>
    <div class="input-group input-group-lg"><input type="text" id="key" name="skey" class="full"  value="<?=@$_SESSION['skey']?>"></div>
    <p class="help-block">从<a href="https://ai.baidu.com/tech/speech/tts" target="_blank">百度云获得</a></p>
  </div>

  <div class="form-group">
    <label for="skey">Server酱 SendKey</label>
    <div class="input-group input-group-lg"><input type="text" id="skey" name="sendkey" class="full" ></div>
    <p class="help-block">从Server酱 sc.ftqq.com 获得。用于完成后的通知</p>
  </div>
  <button type="submit" class="btn btn-default" onclick="this.innerHTML='doing...'">上传</button>
</form>
</div>
