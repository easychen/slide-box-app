<?php
namespace Lazyphp\Controller;
@set_time_limit(0);



class LazyphpController
{
	public function __construct()
    {
        session_start();
    }


    /**
     * 默认提示
     * @ApiDescription(section="Demo", description="默认提示")
     * @ApiLazyRoute(uri="/",method="GET")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function index()
    {
        // echo system("cd /app/ && echo '123' > 'aaa.txt'");exit;
        //phpinfo();return false;

        // $cmd = "cd " . AROOT  . '/app/ppt/ && rm -rf * ';
        // system( $cmd );

        if( file_exists( AROOT . "/ppt/" ) )
            rrmdir(  AROOT . "/ppt/" );

        $_SESSION['done'] = false;    
        
        $data['title'] = $data['top_title'] = 'SlideBox';
        return render( $data );
    }


    /**
     * 默认提示
     * @ApiDescription(section="Demo", description="默认提示")
     * @ApiLazyRoute(uri="/doing",method="GET")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function doing()
    {
        if( $_SESSION['done']  ) 
        return header("Location: /download");

        // return send_result( $_SESSION );
        $data['title'] = $data['top_title'] = '转码';
        
        $apikey = $_SESSION['apikey'];
        if( strlen($apikey) < 1 ) die("apikey cannot be empty");

        return render($data);
    }

    /**
     * 默认提示
     * @ApiDescription(section="Demo", description="默认提示")
     * @ApiLazyRoute(uri="/start",method="GET")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function start()
    {
        $do_sub = intval(v('subtitle'));
        
        $apikey = $_SESSION['apikey'];
        if( strlen($apikey) < 1 ) die("apikey cannot be empty");

        
        
        @unlink( AROOT .  DS . 'log.txt' );

        logit( "分析 pptx 文件" );
        
        system("cd " . AROOT . "/ppt/" . " && soffice --headless --convert-to odp o.pptx");

        system("cd " . AROOT . "/ppt/" . " && unzip o.odp -d odp");
        
        $content_xml = AROOT . "/ppt/odp/content.xml";
        if( $notes_array = get_ppt_notes( $content_xml ))
        {
            logit("notes提取完成，准备图片");

            $bakeup_pdf = AROOT . "/ppt/b.pdf";
            $target_pdf = AROOT . "/ppt/o.pdf";

            if( file_exists( $bakeup_pdf ) )
            {
                copy( $bakeup_pdf , $target_pdf );
            }
            else
            {
                system("cd " . AROOT . "/ppt/" . " && soffice --headless --convert-to pdf o.pptx");
            }

            if( !file_exists( $target_pdf ) )
            {
                logit("目标PDF不存在，进程终止");
                return  false;
            }

            
            
            @mkdir(  AROOT . "/ppt/img" , 0777 , true );
            // 通过 pptx 转化得来
            echo $cmd = "cd " . AROOT . "ppt/" . " && convert o.pdf img/%d.jpg";

            // soffice --headless --convert-to jpg "%F" -outdir img

            system($cmd);

            // 将PDF分解成多张图片
            foreach( $notes_array as $k => $notes_item )
            {
                $base_dir = AROOT . "/ppt/img/";
                $slide_img = $base_dir . $notes_item['id'] . '.jpg';
                if( file_exists( $slide_img ) )
                {
                    $notes_array[$k]['slide'] = 'img/'. $notes_item['id'] . '.jpg';
                }
                else
                {
                    logit( "slide图片不存在" . $slide_img  );
                }
            }

            file_put_contents( AROOT . '/ppt/prepair.json' , json_encode( $notes_array , JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT   ) );

            logit( "数据准备完毕" );

            // 将数据发送到方糖云
            $actions_string = make_post( c("FTCLOUD_API_BASE").'api/actions/'.$apikey , ['json'=>json_encode( $notes_array , JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT   ) ] );

            if( !$info = @json_decode( $actions_string , 1 ) ) die("action数据格式错误".$actions_string);

            if( $info['code'] != 0 || !isset( $info['data'] ) ) die($info['message']);

            $actions = $info['data'];

            file_put_contents( AROOT . '/ppt/actions.json' , json_encode( $actions , JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT   ) );

            logit("Action 分析完成");
            
            // 准备音频
            @mkdir( AROOT . "/ppt/audio" , 0777 , true );
            
            foreach( $actions as $key => $action )
            {
                if( $action['type'] == 'slide'  )
                {
                    if( $_SESSION['engine'] == 'xunfei' )
                    {
                        // 方糖
                        if( $audio_path = text2audio( $apikey , $action['audio_text'] , $action['ai_name'] , $action['ai_speed'] , $action['ai_pitch'] ))
                        {
                            $actions[$key]['ai_audio'] =  'audio/'.basename( $audio_path );
                            $actions[$key]['ai_duration'] = get_video_duration( $audio_path );
                        }
                    }
                    else
                    {
                        // 百度
                        if( $audio_path = text2audio_baidu( $_SESSION['akey'] , $_SESSION['skey'] , $action['audio_text'] , 3 , 7 , 5 ))
                        {
                            $actions[$key]['ai_audio'] =  'audio/'.basename( $audio_path );
                            $actions[$key]['ai_duration'] = get_video_duration( $audio_path );
                        }
                    }
                    
                    
                    
                    
                }
                elseif( $action['type'] == 'video' )
                {
                    $video_path = AROOT . '/ppt/' . $action['path'];
                    if( file_exists( $video_path ) )
                    {
                        $actions[$key]['duration'] = get_video_duration( $video_path ); 
                    }
                }
            }

            file_put_contents( AROOT . '/ppt/action.width.audio.json' , json_encode( $actions , JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT   ) );


        }

        // logit( "<script>alert('完成')</script>" );
        logit( "音频处理完成" );

        $this->video();
    }

     /**
     * 默认提示
     * @ApiDescription(section="Demo", description="默认提示")
     * @ApiLazyRoute(uri="/video",method="GET")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function video()
    {
        $do_sub = intval(v('subtitle'));
        
        $w = '1920';
        $h = '1080';
        $wxh = $w . 'x' . $h;
        
        @unlink( AROOT .  DS . 'log.txt' );
        logit("处理视频");

        $actions = json_decode( file_get_contents( AROOT . '/ppt/action.width.audio.json' ) , true );

        @mkdir( AROOT . "/ppt/video" , 0777 , true );

        // 首先生成合成每一部分的ts文件
        $video_clips = [];
        
        
        include AROOT . '/lib/subtitle.php';

        $s = new \Subtitle();
        $count = count( $actions );
        foreach( $actions as $key => $action )
        {
            if( $action['type'] != 'video' && $action['type'] != 'slide' ) continue;
            
            $ts_file = AROOT . '/ppt/video/'.$key.'.ts';
            $video_clips[] = "file " . $ts_file."\r\n";

            if( $action['type'] == 'video' )
            {
                $s->addLine( "" , $action['duration']*1000 );
                
                $cmd = "cd " . AROOT . '/ppt/' . " && ffmpeg -y -loglevel panic  -i  " . $action['path'] . " -codec:v mpeg2video -vf \"scale=w=" . $w . ":h=" . $h . ":force_original_aspect_ratio=1,pad=" . $w . ":" . $h . ":(ow-iw)/2:(oh-ih)/2\" -qscale:v 2 -s " . $wxh . " -r 25 -acodec libmp3lame -b:a 128000 -ac 1  -ar 16000 " . $ts_file ;

                logit("嵌入".$action['path'].'...');
                system( $cmd );
            }
            elseif( $action['type'] == 'slide' )
            {
                $s->addLine( $action['subtitle'] , $action['ai_duration']*1000 );
                
                $cmd = "cd " . AROOT . '/ppt/' . " && ffmpeg -y -loglevel panic -loop 1 -i " . $action['image'] .  " -i " . $action['ai_audio'] . " -q:v 1 -acodec libmp3lame -b:a  128000 -ac 1  -ar 16000  -shortest -codec:v mpeg2video -vf  format=yuv420p -s " . $wxh . " -r 25 ".$ts_file;
            
                logit("图片音频合成".$action['image'].'...'.($key+1).'/'.$count);
                // logit("cmd = $cmd");
                system( $cmd );
            }
 
        }

        

        $subtitle_file = AROOT . '/ppt/video/final.srt';
        file_put_contents( $subtitle_file , $s->getText() );

        // logit("字幕合成");
        // return ;

        // // 分组合成完毕
        $video_list_file = AROOT . '/ppt/video/list.txt';
        file_put_contents( $video_list_file , join( "" , $video_clips  ) );

        $final = AROOT . '/ppt/video/final.mp4';

        logit("合并视频片段");

        $cmd = "cd " . AROOT . '/ppt/' . " && ffmpeg -y -loglevel panic -safe 0 -f concat -i " . $video_list_file . " -c:v libx264 ".$final ;

        system( $cmd );
        logit( $cmd );

        if( $do_sub > 0 )
        {
            // 压制字幕
            logit("压制字幕");
            $cmd = "cd " . AROOT . '/ppt/video' . " && ffmpeg -y -loglevel panic -i final.srt final.ass && ffmpeg -y -i final.mp4 -vf ass=final.ass  final.srt.mp4";
            logit( $cmd );
            system( $cmd );
        }
        
        logit("完成");
        $_SESSION['done'] = true;
        // logit("<script>location='/download'</script>");

    }

    /**
     * 默认提示
     * @ApiDescription(section="Demo", description="默认提示")
     * @ApiLazyRoute(uri="/send",method="GET")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function send()
    {
        if( strlen($_SESSION['sendkey']) > 3 )
        {
            sc_send("SlideBox转码完成","",t($_SESSION['sendkey']));
        }
    }

    /**
     * 默认提示
     * @ApiDescription(section="Demo", description="默认提示")
     * @ApiLazyRoute(uri="/log",method="GET")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function log()
    {
        $log_file = AROOT . '/log.txt';
        if( file_exists( $log_file ) )
        {
            $lines = explode( "\r\n" , file_get_contents($log_file) );
            
            // print_r( $lines );
            
            $lines = array_reverse( $lines );
            echo join( "<br/>" , $lines ); 
        }
        else
            echo "暂无数据";
        
    }

    /**
     * 默认提示
     * @ApiDescription(section="Demo", description="默认提示")
     * @ApiLazyRoute(uri="/download",method="GET")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function download()
    {
        if( file_exists( AROOT . "/ppt/video/final.srt.mp4" ) )
        {
            $data['url'] =  "/ppt/video/final.srt.mp4" ;
        }
        else
        {
            $data['url'] = "/ppt/video/final.mp4" ;
        }

        $data['title'] = $data['top_title'] = '下载';
        
        return render($data);
    }


    /**
     * 系统提示
     * @ApiDescription(section="Demo", description="系统提示")
     * @ApiLazyRoute(uri="/convert",method="POST")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function convert()
    {
        $apikey = $_REQUEST['apikey'];
        if( strlen($apikey) < 1 ) die("apikey cannot be empty");
        
        $_SESSION['sendkey'] = t(v('sendkey'));
        $_SESSION['engine'] = t(v('engine'));

        if( $_SESSION['engine'] != 'xunfei' )
        {
            $akey = $_REQUEST['akey'];
            if( strlen($akey) < 1 ) die("akey cannot be empty");
            
            $_SESSION['akey'] = t(v('akey'));

            $skey = $_REQUEST['skey'];
            if( strlen($skey) < 1 ) die("skey cannot be empty");
            
            $_SESSION['skey'] = t(v('skey'));
        }


        

        


        
        if( isset( $_FILES['pptx'] ) && $_FILES['pptx']['error'] == 0 )
        {
            // 处理
            // say( "处理上传文件" );
            @mkdir( AROOT . "/ppt" , 0777 , true );

            $pptx = AROOT . '/ppt/o.pptx';
            move_uploaded_file( $_FILES['pptx']['tmp_name'] , $pptx );

            if( isset( $_FILES['pdf'] ) && $_FILES['pdf']['error'] == 0 )
            {
                move_uploaded_file( $_FILES['pdf']['tmp_name'] , AROOT . '/ppt/b.pdf' );
            }

            // setcookie( "apikey" , $apikey , time()+60*60*24*30 );
            $_SESSION['apikey'] = $apikey;
            
            // 转向
            // echo "<script>window.location='/doing'<script>";
            header( "location: /doing" );
            return true;



            


        }
        
        
        // print_r($_FILES);
        
        
        // echo "doing";
        // flush();
        // echo "done";
    }

    /**
     * 系统提示
     * @ApiDescription(section="Demo", description="系统提示")
     * @ApiLazyRoute(uri="/encode",method="POST")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function encode()
    {
        // 
    }



    /**
     * 系统提示
     * @ApiDescription(section="Demo", description="系统提示")
     * @ApiLazyRoute(uri="/info",method="GET")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function info()
    {
        //$data['notice'] = ;
        return send_error('SYSTEM','这里是信息提示页面');
    }
    
    /**
     * Demo接口
     * @ApiDescription(section="Demo", description="乘法接口")
     * @ApiLazyRoute(uri="/demo/times",method="GET")
     * @ApiParams(name="first", type="string", nullable=false, description="first", check="check_not_empty", cnname="第一个数")
     * @ApiParams(name="second", type="string", nullable=false, description="second", check="check_not_empty", cnname="第二个数")
     * @ApiReturn(type="object", sample="{'code': 0,'message': 'success'}")
     */
    public function demo($first,$second)
    {
        return send_result(intval($first)*intval($second));
    }

}
