<?php
// 检查并补全必须的常量
if( !defined('DS') ) define( 'DS' , DIRECTORY_SEPARATOR );
if( !defined('AROOT') ) define( 'AROOT' ,  __DIR__ . DS );

// 定义常用跟路径
define( 'FROOT' , __DIR__ . DS . '_lp'. DS );

// 设置时区
@date_default_timezone_set('Asia/Chongqing');


// 载入composer autoload
require AROOT . 'vendor' . DS . 'autoload.php';

require_once FROOT . 'lib' . DS . 'functions.php'; // 公用函数
require_once FROOT . 'config' . DS . 'core.php'; // 核心配置
require_once AROOT . 'config' . DS . 'database.php'; // 数据库配置
require_once AROOT . 'lib' . DS . 'functions.php'; // 公用函数
require_once AROOT . 'config' . DS . 'app.php'; // 应用配置
require_once AROOT . 'lib' . DS . 'functions.php'; // 公用函数

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    /**
     * 解压上传的文件。
     */
    public function buildUnzip()
    {
        echo c("FTCLOUD_API_BASE");
    }

    public function test()
    {
        print_r( get_video_duration( AROOT . '/ppt/audio/AUDIO.cd26a02bb5b7edd6630c40b18b0688ed.mp3' ));
        //echo text2audio( "FTCVOICE11864-0PCEXHY0V5E0P17" , "却回不到从前" );
    }
}