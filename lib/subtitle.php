<?php

class Subtitle
{
    public function __construct()
    {
        $this->start = 0;
        $this->lines = [];
    }

    public function addLine( $text , $ms )
    {
        $this->lines[] = ['text'=>$text,'ms'=>$ms];
        // logit("add line");
    }

    public function getText()
    {
        $ret = '';

        foreach( $this->lines as $key => $line )
        {
            $ret .= ($key+1) . "\r\n" . $this->mstotime( $this->start ) . " --> " ;

            $this->start = $this->start + $line['ms'];

            $ret .= $this->mstotime( $this->start ) . "\r\n" . $this->text2sub( $line['text'] ). "\r\n\r\n";

            // $this->start += 500; // 每句话间隔500ms
        }

        return $ret;
    }

    private function text2sub( $text )
    {
        // 字幕规范化，加入英文空格以支持自然换行。
        $text = str_replace( "," , ", " , $text);
        $text = str_replace( "，" , ", " , $text);
        return $text;
    }

    private function mstotime( $ms )
    {
        // 小时
        $h = intval( $ms / ( 1000*60*60 ) );
        $ms =$ms %  ( 1000*60*60 ); 

        $m = intval( $ms / ( 1000*60 ) );
        $ms =$ms %  ( 1000*60 ); 

        $s = intval( $ms / ( 1000 ) );
        $ms = $ms % 1000;

        $ms = intval( $ms );

        return  sprintf( "%02d" , $h ) . ":" . sprintf( "%02d" , $m ) . ":" . sprintf( "%02d" , $s ) . "," . $ms ;
    }
}