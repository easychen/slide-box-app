<?php

function get_ppt_notes( $content_xml )
{
    $content = file_get_contents( $content_xml ) ;

    $reg = '/<draw:page.+?draw:name="(.+?)".+?>[\s\S]*?<presentation:notes.*?>([\s\S]*?)<\/presentation:notes>[\s\S]*?<\/draw:page>/i';


    if( preg_match_all( $reg , $content , $out ) )
    {
        $ret = [];
        $count = count( $out[0] );
        for( $i = 0 ; $i < $count ; $i++ )
        {
            $ret[$i] = [ 'id' => $i , 'name' => $out[1][$i] , 'notes' => strip_tags($out[2][$i]) ];

        }

        return $ret;
        // return  json_encode( $ret , JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT   ) ;
    }
    else
    return false;
}

function rrmdir($src) {
    $dir = opendir($src);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            $full = $src . '/' . $file;
            if ( is_dir($full) ) {
                rrmdir($full);
            }
            else {
                unlink($full);
            }
        }
    }
    closedir($dir);
    rmdir($src);
}

function text2audio_baidu( $akey , $skey , $text , $name = '3' , $speed = '5' , $pitch = '5' )
{
    $key = $text . '-' . $name . '-' . $speed . '-' . $pitch;

    $file_name = AROOT . DS . 'ppt' . DS . 'audio' . DS . 'AUDIO.BD.' . md5( $key ) . '.mp3';

    if( !file_exists(  $file_name ) )
    {
        if( !isset( $_SESSION['token'] ) )
        {
            logit( "Token 不存在，换取 token" );
            $ret = file_get_contents( "https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=" . urlencode( $akey ) . "&client_secret=" . urlencode( $skey ) );

            $bad_token = true;
            
            if( $ret )
            {
                if(  $info = json_decode( $ret , 1 ) )
                {
                    if( isset( $info['access_token'] ) ) 
                    {
                    logit("换取Token成功");
                        $_SESSION['token'] = $info['access_token'];
                        $bad_token = false;
                    }
                }    
            }

            if( $bad_token )
            {
                logit("换取Token失败");
                return false;
            }
        }

        $re_try = 0;

        get_audio:
        
        $audio = file_get_contents( 'http://tsn.baidu.com/text2audio?lan=zh&ctp=1&cuid=LOCALMACSLIDEBOX1066&tok=' . urlencode( $_SESSION['token'] ) . '&tex=' . urlencode( urlencode( $text ) ) . '&vol=9&per=' . intval( $name ) . '&spd=' . $speed . '&pit=' . $pitch );

        $headers = parseHeaders( $http_response_header );
        if( $headers['Content-Type'] == 'audio/mp3' )
        {
            
            file_put_contents( $file_name , $audio );
            logit("🎧 正在转换：" . mb_substr( $text , 0 , 10 )."...");
            return $file_name;
        }
        else
        {
            //logit( "音频转码失败，转换中止" );
            $re_try++;
            logit( $audio , 1 );
            
            if( $re_try < 2 ) goto get_audio;
            else return false;
        }
    }
    else
        return $file_name;
}

function text2audio( $apikey , $text , $name = 'x_yifeng' , $speed = '70' , $pitch= '50' )
{
    $key = $text . '-' . $name . '-' . $speed . '-' . $pitch;

    $file_name = AROOT . DS . 'ppt' . DS . 'audio' . DS . 'AUDIO.' . md5( $key ) . '.mp3';

    if( !file_exists(  $file_name ) )
    {
        $content = make_post( c("FTCLOUD_API_BASE").'api/tts/'.$apikey , [ 'text' => $text , 'name' => $name , 'speed' => $speed , 'pitch' => $pitch ]  );

        if( strlen( $content ) < 200 )
        {
            $info = json_decode( $cotnent , 1 );
            if( $info['error'] )
            {
                logit("音频转码错误".$cotnent);
                return  false;
            }
        }

        file_put_contents( $file_name , $content  );
        logit("🎧 正在转换：" . mb_substr( $text , 0 , 10 )."...");
    }
    else
    {
        logit("🚀 已存在，跳过: " . mb_substr( $text , 0 , 10 )."..."); 
    }

    
    return $file_name ;
}

function get_video_duration($video, $ffmpeg = '/usr/bin/ffmpeg') 
{

    $command = $ffmpeg . ' -i ' . $video . ' -vstats 2>&1';
    $output = shell_exec($command);

    // $regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/"; // or : $regex_sizes = "/Video: ([^\r\n]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/"; (code from @1owk3y)
    // if (preg_match($regex_sizes, $output, $regs)) {
    //     $codec = $regs [1] ? $regs [1] : null;
    //     $width = $regs [3] ? $regs [3] : null;
    //     $height = $regs [4] ? $regs [4] : null;
    // }

    $regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
    if (preg_match($regex_duration, $output, $regs)) {
        $du = intval( $regs [1] ) * 60 * 60 
             +intval( $regs [2] ) * 60  
             +intval( $regs [3] )
             +intval( $regs [4] )/1000;
    }

    return $du;
}

function parseHeaders( $headers )
{
    $head = array();
    foreach( $headers as $k=>$v )
    {
        $t = explode( ':', $v, 2 );
        if( isset( $t[1] ) )
            $head[ trim($t[0]) ] = trim( $t[1] );
        else
        {
            $head[] = $v;
            if( preg_match( "#HTTP/[0-9\.]+\s+([0-9]+)#",$v, $out ) )
                $head['reponse_code'] = intval($out[1]);
        }
    }
    return $head;
}

function say( $something )
{
    echo $something."<br/>\r\n";
    flush();
    // ob_end_flush();
}

function logit( $text )
{
    file_put_contents( AROOT .  DS . 'log.txt' , $text . "<br/>\r\n" , FILE_APPEND );
}


function table( $name )
{
    if( !isset( $GLOBALS['LP_LDO_'.$name] ) )
    {
        $GLOBALS['LP_LDO_'.$name] = new \Lazyphp\Core\Ldo($name);
    }

    return $GLOBALS['LP_LDO_'.$name];
}

function lp_throw( $type , $info , $args = null )
{
    if( !is_array( $args )) $args = [ $args ] ;
    $code = isset( c('error_type')[$type] ) ? c('error_type')[$type] : 99999;
    $message = '[' . $type . ']' . sprintf( $info , ...$args );
    throw new \Lazyphp\Core\LpException( $message , $code , $info , $args );
}

function lp_now()
{
    return date("Y-m-d H:i:s");
}

function lp_uid()
{
    return isset( $_SESSION['uid'] ) ? intval( $_SESSION['uid'] ) : 0;
}

// ======================

function render( $data = NULL , $layout = NULL , $sharp = 'default' )
{
	if( $layout == null )
	{
		if( is_ajax_request() )
		{
			$layout = 'ajax';
		}
		elseif( is_mobile_request() )
		{
			$layout = 'mobile';
		}
		else
		{
			$layout = 'web';
		}
	}
	
	$GLOBALS['layout'] = $layout;
	$GLOBALS['sharp'] = $sharp;
	
	$layout_file = AROOT . 'view/' . $layout . '/' . $sharp . '.tpl.php';
    
    if( file_exists( $layout_file ) )
	{
		@extract( $data );
		require( $layout_file );
	}
	else
	{
		$layout_file = FROOT . 'view/' . $layout . '/' . $sharp .  '.tpl.php';
		if( file_exists( $layout_file ) )
		{
			@extract( $data );
			require( $layout_file );
		}	
	}
}

function info_page( $info , $title = '系统消息' )
{
	if( is_ajax_request() )
		$layout = 'ajax';
	else
		$layout = 'web';
	
	$data['top_title'] = $data['title'] = $title;
	$data['info'] = $info;
	
	render( $data , $layout , 'info' );
	
}

function is_mobile_request()
{
    $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';
 
    $mobile_browser = '0';
 
    if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
        $mobile_browser++;
 
    if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false))
        $mobile_browser++;
 
    if(isset($_SERVER['HTTP_X_WAP_PROFILE']))
        $mobile_browser++;
 
    if(isset($_SERVER['HTTP_PROFILE']))
        $mobile_browser++;
 
    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
    $mobile_agents = array(
                        'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
                        'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
                        'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
                        'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
                        'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
                        'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
                        'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
                        'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
                        'wapr','webc','winw','winw','xda','xda-'
                        );
 
    if(in_array($mobile_ua, $mobile_agents))
        $mobile_browser++;
 
    if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false)
        $mobile_browser++;
 
    // Pre-final check to reset everything if the user is on Windows
    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false)
        $mobile_browser=0;
 
    // But WP7 is also Windows, with a slightly different characteristic
    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false)
        $mobile_browser++;
 
    if($mobile_browser>0)
        return true;
    else
        return false;
}

function make_post( $url , $data )
{
	// 如果请求本站本身的接口，需要把下边的session关掉
	// 否则会导致Session文件锁死
	// @session_write_close(); 
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
    //$ch = apply_filter( 'UPLOAD_CURL_SETTINGS' , $ch );
    $response = curl_exec($ch);
    return $response;
}

function sc_send(  $text , $desp = '' , $key = '[SCKEY(登入后可见)]'  )
{
	$postdata = http_build_query(
    array(
        'text' => $text,
        'desp' => $desp
    )
);

$opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $postdata
    )
);
$context  = stream_context_create($opts);
return $result = file_get_contents('https://sc.ftqq.com/'.$key.'.send', false, $context);

}






